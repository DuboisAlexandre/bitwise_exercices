/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_bits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 20:41:44 by adubois           #+#    #+#             */
/*   Updated: 2016/03/21 21:18:15 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

unsigned char	reverse_bits(unsigned char octet)
{
	int				i;
	unsigned char	result;

	result = 0;
	i = 0;
	while (i < 8)
	{
		result <<= 1;
		if ((octet >> i) % 2)
			++result;
		++i;
	}
	return (result);
}

int				main(void)
{
	unsigned char	octet;

	octet = 65;
	printf("%d => %d\n", octet, reverse_bits(octet));
	return (0);
}

