/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 20:41:44 by adubois           #+#    #+#             */
/*   Updated: 2016/03/21 21:23:15 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	print_bits(unsigned char octet)
{
	int		i;

	i = 7;
	while (i >= 0)
	{
		if ((octet >> i) % 2)
			write(1, "1", 1);
		else
			write(1, "0", 1);
		--i;
	}
}

int		main(void)
{
	unsigned char	octet;

	octet = 65;
	print_bits(octet);
	printf(" => %d\n", octet);
	return (0);
}

